package com.marrowbone.gsmsignalstrength;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	/*
	 * This variables need to be global, so we can used them onResume and
	 * onPause method to stop the listener
	 */
	TelephonyManager Tel;
	MyPhoneStateListener MyListener;

	TextView mSignalStrengthTextView1;
	TextView mSignalStrengthTextView2;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/* Update the listener, and start it */
		MyListener = new MyPhoneStateListener();
		Tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		Tel.listen(MyListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

		mSignalStrengthTextView1 = (TextView) findViewById(R.id.evdo_rssi);
		mSignalStrengthTextView2 = (TextView) findViewById(R.id.signal_strength);
	}

	/* Called when the application is minimized */
	@Override
	protected void onPause() {
		super.onPause();
		Tel.listen(MyListener, PhoneStateListener.LISTEN_NONE);
	}

	/* Called when the application resumes */
	@Override
	protected void onResume() {
		super.onResume();
		Tel.listen(MyListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
	}

	/* —————————– */
	/* Start the PhoneState listener */
	/* —————————– */
	private class MyPhoneStateListener extends PhoneStateListener {
		/*
		 * Get the Signal strength from the provider, each tiome there is an
		 * update
		 */
		@Override
		public void onSignalStrengthsChanged(SignalStrength signalStrength) {
			super.onSignalStrengthsChanged(signalStrength);
			if (signalStrength.isGsm()) {
				mSignalStrengthTextView1.setText(String.valueOf(signalStrength.getCdmaDbm()));
				mSignalStrengthTextView2.setText(String.valueOf(signalStrength.getGsmSignalStrength()));
				Toast.makeText(getApplicationContext(), "updated",
						Toast.LENGTH_SHORT).show();

			} else {
				Toast.makeText(getApplicationContext(), "no GSM",
						Toast.LENGTH_SHORT).show();
			}
		}
	}
}
